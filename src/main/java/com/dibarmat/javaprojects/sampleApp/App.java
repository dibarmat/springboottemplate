package com.dibarmat.javaprojects.sampleApp;

import org.springframework.boot.SpringApplication;  
import org.springframework.boot.autoconfigure.SpringBootApplication;  

@SpringBootApplication  //Basic tag needed to make our root-class a spring-boot project
public class App {
	
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);   //We put a Spring application wrapper around the app.class (root class).  
	} //With the 'SpringBootApplication' wrapper applied above, it wraps around the 'SpringApplicatin' reference and boot-straps the application making it a spring-boot app.

}
